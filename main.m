clear all
clc

addpath('export_fig/')
global solvent_type solvent phi_toggle eps_in conv_factor FID errorgraph Ion_Data EXP_G_dat EXP_S_dat EXP_HEATCAP_dat FID_PREDICT Ion_Color total_solvent_info FID2 printOn hfuncgraph

% set the PrintOn to 0 (zero) if you don't want to save each
% individual figure automatically, according to the names set
% inside the code.

printOn = 1;
if printOn
    legaxes=0;
else
    legaxes=1;
end

% set the following to 0 (zero) if you don't want to plot
% the corresponding figures

heatcapgraph=0;
deltaggraph=0;
deltasgraph=0;
 

FID=fopen('param_data.txt','w');
FID_PREDICT=fopen('predict.txt','w');
FID2=fopen('parinfo.txt','w');

solvent_type = {'W','MeOH','AN','PC'}; % You can pick just one or any combination of these solvents

solvent = read_solvent_data('Solvents.txt',solvent_type);

eps_in=1;
conv_factor = 332.112 * 4.184;      % from Angstroms/relative
                                    % permittivities/electron charges
                                    % into kJ/mol;
                                    





for i = 1:length(solvent)
    
clearvars   dphidn_HSBC_MSA sigma_over_dphidn_HSBC_MSA ...
            dphidn_NLBC_TANH sigma_nom_over_dphidn_NLBC_TANH...
            dphidn_BORN sigma_over_dphidn_BORN ...
            DG_HSBC_MSA DS_HSBC_MSA...
            DG_Fawcett_Asym DS_Fawcett_Asym dphidn_Fawcett_Asym sigma_over_dphidn_Fawcett_Asym...
            DG_Fawcett_Sym DS_Fawcett_Sym dphidn_Fawcett_Sym sigma_over_dphidn_Fawcett_Sym...
            DG_NLBC_TANH DS_NLBC_TANH...        
            DG_BORN DS_BORN CP_BORN...
            CP_NLBC_TANH 


for kk=1:2
    phi_toggle=kk-1;
    [       dphidn_HSBC_MSA(:,:,kk),    sigma_over_dphidn_HSBC_MSA(:,:,kk),     ...
            dphidn_NLBC_TANH(:,:,kk),   sigma_nom_over_dphidn_NLBC_TANH(:,:,kk),...
            dphidn_BORN(:,:,kk),        sigma_over_dphidn_BORN(:,:,kk),         ...
            DG_HSBC_MSA(:,:,kk),        DS_HSBC_MSA(:,:,kk),...
            DG_Fawcett_Asym(:,:,kk),    DS_Fawcett_Asym(:,:,kk),                dphidn_Fawcett_Asym(:,:,kk),    sigma_over_dphidn_Fawcett_Asym(:,:,kk),...
            DG_Fawcett_Sym(:,:,kk),     DS_Fawcett_Sym(:,:,kk),                 dphidn_Fawcett_Sym(:,:,kk),     sigma_over_dphidn_Fawcett_Sym(:,:,kk),...
            DG_NLBC_TANH(:,:,kk),       DS_NLBC_TANH(:,:,kk),...        
            DG_BORN(:,:,kk),            DS_BORN(:,:,kk),                        CP_BORN(:,:,kk),...
            CP_NLBC_TANH(:,:,kk)] = Calcsolventmodels(i);

    Predictsolventmodels(i);
    
    creatparamtable(total_solvent_info(i),phi_toggle)
end



TABLE_G=zeros(length(Ion_Data),6,length(solvent));
TABLE_S=zeros(length(Ion_Data),6,length(solvent));
TABLE_H=zeros(length(Ion_Data),3,length(solvent));
    

        TABLE_G(:,1,i)=DG_BORN(:,:,1);         % Delta G for Born 
        TABLE_S(:,1,i)=DS_BORN(:,:,1);         % Delta S for Born
        TABLE_G(:,2,i)=DG_Fawcett_Sym(:,:,1);  % Delta G for Symmetric Fawcett
        TABLE_S(:,2,i)=DS_Fawcett_Sym(:,:,1);  % Delta S for Symmetric Fawcett
        TABLE_G(:,3,i)=DG_Fawcett_Asym(:,:,1); % Delta G for Asymmetric Fawcett
        TABLE_S(:,3,i)=DS_Fawcett_Asym(:,:,1); % Delta S for Asymmetric Fawcett
        TABLE_G(:,4,i)=DG_HSBC_MSA(:,:,1);     % Delta G for SLIC/MSA
        TABLE_S(:,4,i)=DS_HSBC_MSA(:,:,1);     % Delta G for SLIC/MSA
        TABLE_G(:,5,i)=DG_NLBC_TANH(:,:,1);    % Delta G for NLBC w/o phi_stat
        TABLE_S(:,5,i)=DS_NLBC_TANH(:,:,1);    % Delta S for NLBC w/o phi_stat      
        TABLE_G(:,6,i)=DG_NLBC_TANH(:,:,2);    % Delta G for NLBC w phi_stat
        TABLE_S(:,6,i)=DS_NLBC_TANH(:,:,2);    % Delta S for NLBC w phi_stat
        TABLE_H(:,1,i)=CP_BORN(:,:,1);          % Heat Capacity for Born
        TABLE_H(:,2,i)=CP_NLBC_TANH(:,:,1);    % Heat Capacity for NLBC w/o phi_stat
        TABLE_H(:,3,i)=CP_NLBC_TANH(:,:,2);    % Heat Capacity for NLBC w phi_stat
        
        ERROR_G_WO_PHI=TABLE_G(:,5,i)-EXP_G_dat';
        ERROR_G_W_PHI=TABLE_G(:,6,i)-EXP_G_dat';
        
        ERROR_S_WO_PHI=TABLE_S(:,5,i)-EXP_S_dat';
        ERROR_S_W_PHI=TABLE_S(:,6,i)-EXP_S_dat';
        
        ERROR_C_WO_PHI=TABLE_H(:,2,i)-EXP_HEATCAP_dat';
        ERROR_C_W_PHI=TABLE_H(:,3,i)-EXP_HEATCAP_dat';
        
        RMS(i,1)=rms(ERROR_G_WO_PHI);
        RMS(i,2)=rms(ERROR_G_W_PHI);
        
        RMS(i,3)=rms(ERROR_S_WO_PHI);
        RMS(i,4)=rms(ERROR_S_W_PHI);
        
        RMS(i,5)=rms(ERROR_C_WO_PHI);
        RMS(i,6)=rms(ERROR_C_W_PHI);
        
        
 map=[0,0,0; 1,0,0; 0.5, 0, 0; 0.5, 0.5, 0; 0, 0.5, 0; 0, 0.5, 1; 0, 0, 0.5; 0.5, 0, 0.5; 0.5, 0.5 0.5]; 
 %map = map(Ion_Color,:);

if deltaggraph==1
    figure 
    scatter(EXP_G_dat',TABLE_G(:,1,i),400*ones(1,length(Ion_Color)),Ion_Color,'s','linewidth',3);
    hold on
    scatter(EXP_G_dat',TABLE_G(:,5,i),400*ones(1,length(Ion_Color)),Ion_Color,'o','linewidth',3);
    colormap(map);
    hold on
    scatter(EXP_G_dat',TABLE_G(:,6,i),500*ones(1,length(Ion_Color)),Ion_Color,'*','linewidth',3);
    colormap(map);
    hold on
    scatter(EXP_G_dat',TABLE_G(:,3,i),400*ones(1,length(Ion_Color)),Ion_Color,'+','linewidth',3);
    colormap(map);
    hold on
    plot([min(EXP_G_dat)-1000 max(EXP_G_dat)+1000],[min(EXP_G_dat)-1000 max(EXP_G_dat)+1000],'k','linewidth',2);
    ax = gca;
    ax.LineWidth = 1;
    ax.Box = 'on';
    set(gca,'fontsize',30);
    fig = gcf;
    fig.PaperUnits = 'inches';
    fig.PaperPosition = [0 0 6 6];
    axis([min(EXP_G_dat)-20 max(EXP_G_dat)+20 min(min(TABLE_G(:,:,i)))-20 max(max(TABLE_G(:,:,i)))+20])


    if legaxes
        xlabel(['Experimental \DeltaG for ', total_solvent_info.Name,' (kJ/mol)'])
        ylabel(['Computed \DeltaG for ', total_solvent_info.Name,' (kJ/mol)'])
        legend('Classical Born','SLIC/MSA','SLIC/tanh (\phi^{static} excluded)','SLIC/tanh (\phi^{static} included)','Asymmetric MSA (Fawcett)','Location','southeast'); 
    end


    if printOn
      filename = sprintf('deltaG-%s.PDF',solvent(i).Name);
      export_fig(filename,'-painters','-transparent');
    end
end

if deltasgraph==1
    figure
    scatter(EXP_S_dat',TABLE_S(:,1,i),400*ones(1,length(Ion_Color)),Ion_Color,'s','linewidth',3);
    hold on
    scatter(EXP_S_dat',TABLE_S(:,5,i),400*ones(1,length(Ion_Color)),Ion_Color,'o','linewidth',3);
    colormap(map);
    hold on
    scatter(EXP_S_dat',TABLE_S(:,6,i),500*ones(1,length(Ion_Color)),Ion_Color,'*','linewidth',3);
    colormap(map);
    hold on
    scatter(EXP_S_dat',TABLE_S(:,3,i),400*ones(1,length(Ion_Color)),Ion_Color,'+','linewidth',3);
    colormap(map);
    hold on
    plot([min(EXP_S_dat)-1000 max(EXP_S_dat)+1000],[min(EXP_S_dat)-1000 max(EXP_S_dat)+1000],'k','linewidth',2);

    ax = gca;
    ax.LineWidth = 1;
    ax.Box = 'on';
    set(gca,'fontsize',30);
    fig = gcf;
    fig.PaperUnits = 'inches';
    fig.PaperPosition = [0 0 6 6];
    
    if legaxes
        xlabel(['Experimental \DeltaS for ', total_solvent_info.Name,' (J/mol)'])
        ylabel(['Computed \DeltaS for ', total_solvent_info.Name,' (J/mol)'])
        legend('Classical Born','SLIC/MSA','SLIC/tanh (\phi^{static} excluded)','SLIC/tanh (\phi^{static} included)','Asymmetric MSA (Fawcett)','Location','southeast'); 
    end
    
    axis([min(EXP_S_dat)-20 max(EXP_S_dat)+20 min(min(TABLE_S(:,:,i)))-20 max(max(TABLE_S(:,:,i)))+20])
    
    if printOn
      filename = sprintf('deltaS-%s.PDF',solvent(i).Name);
      export_fig(filename,'-painters','-transparent');
    end
end

if heatcapgraph==1
    figure
    scatter(EXP_HEATCAP_dat',TABLE_H(:,1,i),400*ones(1,length(Ion_Color)),Ion_Color,'s','linewidth',3);
    hold on
    scatter(EXP_HEATCAP_dat',TABLE_H(:,2,i),400*ones(1,length(Ion_Color)),Ion_Color,'o','linewidth',3);
    colormap(map);
    hold on
    scatter(EXP_HEATCAP_dat',TABLE_H(:,3,i),500*ones(1,length(Ion_Color)),Ion_Color,'*','linewidth',3);
    colormap(map);
    hold on
    plot([min(EXP_HEATCAP_dat)-1000 max(EXP_HEATCAP_dat)+1000],[min(EXP_HEATCAP_dat)-1000 max(EXP_HEATCAP_dat)+1000],'k','linewidth',2);
    ax = gca;
    ax.LineWidth = 1;
    ax.Box = 'on';
    set(gca,'fontsize',30);
    fig = gcf;
    fig.PaperUnits = 'inches';
    fig.PaperPosition = [0 0 6 6];
    
    if legaxes
        xlabel(['Experimental C_{p} for ', total_solvent_info.Name,' (J/mol/K)'])
        ylabel(['Computed C_{p} for ', total_solvent_info.Name,' (J/mol/K)']) 
        legend('Classical Born','SLIC/MSA','SLIC/tanh (\phi^{static} excluded)','SLIC/tanh (\phi^{static} included)','Asymmetric MSA (Fawcett)','Location','southeast'); 
    end        
     axis([min(EXP_HEATCAP_dat)-20 max(EXP_HEATCAP_dat)+20 min(min(TABLE_H(:,:,i)))-20 max(max(TABLE_H(:,:,i)))+20])
    
     if strcmp(solvent(i).Name,'W')
        axis([-120 20 -100 20]);
     end
     if strcmp(solvent(i).Name,'MeOH')
        axis([-120 70 -300 100]);
     end
     if strcmp(solvent(i).Name,'EtOH')
        axis([-200 150 -450 150]);
     end
     if strcmp(solvent(i).Name,'F')
        axis([-20 100 -250 150]);
     end  
     if strcmp(solvent(i).Name,'AN')
        axis([20 100 -700 500]);
    end    
     if strcmp(solvent(i).Name,'DMF')
        axis([-20 120 -90 120]);
    end
    if strcmp(solvent(i).Name,'DMSO')
        axis([-60 130 -100 200]);
    end
    if strcmp(solvent(i).Name,'NM')
        axis([-20 220 -130 220]);
    end
    if strcmp(solvent(i).Name,'PC')
        axis([20 60 -200 150]);
    end

    if printOn
      filename = sprintf('CP-%s.pdf',solvent(i).Name);
      export_fig(filename,'-painters','-transparent');
    end
end


end
