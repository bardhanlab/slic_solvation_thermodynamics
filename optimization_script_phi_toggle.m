function total_solvent_info=optimization_script_phi_toggle(index)

global phi_toggle solvent FID errorgraph 

offset = +0.99;

if phi_toggle==0
par_num=4;
    if strcmp(solvent(index).Name,'DMA') || strcmp(solvent(index).Name,'HMPA') || strcmp(solvent(index).Name,'NMP')
        x_init = [0 0 0 0];
    elseif strcmp(solvent(index).Name,'DMSO')
        x_init = [-0.24 5.13 2.65 -0.62];
    elseif strcmp(solvent(index).Name,'AN')
%         x_init = [.2 80 40 .8      ];
%         x_init = [0.07858793 80.23646103 39.51139320 0.44508171 ];
        x_init = [0.07858793-offset 80.23646103-offset 39.51139320-offset 0.44508171-offset ];
        x_init = [offset -1+offset -1+offset -1+offset];
%         x_init = [0.09767731-offset    80.14314198-offset    39.59964438+offset      0.46309092+offset  ];        
    elseif strcmp(solvent(index).Name,'F')
        x_init = [0.24 5.13 2.65 0];
    elseif strcmp(solvent(index).Name,'PrOH')
        x_init = [0.24 -5.13 2.65 0.62];
    else
        x_init=[0.24 5.13 2.65 0.62];
%         x_init=[-0.24 -5.13 -2.65 0.62];
    end

elseif phi_toggle==1
par_num=5;
    if strcmp(solvent(index).Name,'DMA') || strcmp(solvent(index).Name,'HMPA')
        x_init = [0 0 0 0 0];
    elseif strcmp(solvent(index).Name,'PrOH')
        x_init = [0.11151568 40.96797198 20.40752039 0.47724794 0 ];
    elseif strcmp(solvent(index).Name,'AN')
%         x_init = [.2 80 40 .8 0];
%         x_init = [0.07858793 80.23646103 39.51139320 0.44508171 0.00000000];
%         x_init = [0 0 0 0 0];
        x_init = [1.09767731-offset    80.14314198-offset    39.59964438      0.46309092+offset 0.00000000];
    elseif strcmp(solvent(index).Name,'F')
        x_init = [0.24 5.13 2.65 0 0];
    else
        x_init = [0.11151568 40.96797198 20.40752039 0.47724794 0];
%         x_init = [-0.11151568 -40.96797198 -20.40752039-0.47724794 0];
%         x_init = [0 0 0 0 0];

    end

end
% My test fot initial guess
if phi_toggle==0
    par_num=4;
    if strcmp(solvent(index).Name,'W')
        x_init = [0.08511475 41.59557664 19.91115710 0.38971054];
    elseif strcmp(solvent(index).Name,'MeOH')
        x_init = [0.06356225 34.26443441 16.60661404 0.39490412];
    elseif strcmp(solvent(index).Name,'EtOH')
         x_init = [0.05717067 20.69582912 10.18201962 0.40506834];
    elseif strcmp(solvent(index).Name,'PrOH')
         x_init = [0.05381134 11.15859295 9.99999976 0.46430932];
    elseif strcmp(solvent(index).Name,'F')
        x_init = [0.05381134 11.15859295 9.99999976 0.46430932];
        %x_init = [0.08511475 41.59557664 19.91115710 0.38971054];
    elseif strcmp(solvent(index).Name,'AN')
        %x_init = [0.07 10 -1e-6 0.4];
        x_init = [0.08511475 41.59557664 19.91115710 0.38971054];
    elseif strcmp(solvent(index).Name,'DMF')
        x_init = [0.04583387 31.90680303 16.16964709 0.37633688];
    elseif strcmp(solvent(index).Name,'DMSO')
        x_init = [0.03946971 40.99999510 21.00001036 0.38068341];
    elseif strcmp(solvent(index).Name,'NM')
        x_init = [0.10312323 40.91318796 20.51769096 0.49249260];
    elseif strcmp(solvent(index).Name,'PC')
        x_init=[0.08952048 41.07673549 20.18259508 0.44728357];
    end

elseif phi_toggle==1
    par_num=5;
    if strcmp(solvent(index).Name,'W')
        x_init = [0.09963186    41.24662749    19.82524588      0.40670284      -0.00796158];
    elseif strcmp(solvent(index).Name,'MeOH')
        x_init = [0.08659381    41.20043006    19.93193644      0.40887808      -0.00933893];
    elseif strcmp(solvent(index).Name,'EtOH')
         x_init = [0.08140513    41.12844475    20.08284203      0.41970301      -0.00960579];
    elseif strcmp(solvent(index).Name,'PrOH')
         x_init = [0.11151568    40.96797198    20.40752039      0.47724794      -0.00960579];
    elseif strcmp(solvent(index).Name,'F')
        x_init = [0.05329142    16.30016742    11.50158980      0.39853810      -0.00796158];
        %x_init = [0.09963186    41.24662749    19.82524588      0.40670284      -0.00796158];
    elseif strcmp(solvent(index).Name,'AN')
        x_init = [0.07887919 41.31332860 20.48279204 0.44486734 -0.01712045];
        %x_init = [0.09963186    41.24662749    19.82524588      0.40670284      -0.00796158];
    elseif strcmp(solvent(index).Name,'DMF')
        x_init = [0.09963186    41.24662749    19.82524588      0.40670284      -0.02471057];
    elseif strcmp(solvent(index).Name,'DMSO')
        x_init = [0.09383908    41.46343372    20.03845590      0.41441031      -0.02281930];
        %x_init = [0.09377549    41.52356393    20.06594877      0.41431348      -0.01281816];
    elseif strcmp(solvent(index).Name,'NM')
        x_init = [0.14994380    40.85719017    20.62247075      0.52262804      -0.01596521];
    elseif strcmp(solvent(index).Name,'PC')
        x_init=[0.12875132    40.99481217    20.35430599      0.49298944      -0.01904414 ];
    end
end





solvent_opt_data=zeros(length(solvent),3*par_num);
num_temps=8;


if phi_toggle == 0
Params = zeros(num_temps,par_num,length(solvent));
fprintf('%5s \n','Optimizing WITHOUT phi stat');
fprintf('%5s \n','-----------------------------');
fprintf('\n');
else
Params = zeros(num_temps,par_num,length(solvent));
fprintf('%5s \n','Optimizing WITH phi stat');
fprintf('%5s \n','-----------------------------');
fprintf('\n');
end

t=linspace(solvent(index).Tmin,solvent(index).Tmax,num_temps);

for j = 1:length(t)
    Params(j,:,index) = mytanhopt_temp(t(j),index,x_init);
end


fprintf('\n');
for k=1:par_num
    fitparams=polyfit(t,Params(:,k,index)',3);
    solvent_opt_data(index,(k-1)*4+1)=fitparams(1);
    solvent_opt_data(index,(k-1)*4+2)=fitparams(2);
    solvent_opt_data(index,(k-1)*4+3)=fitparams(3);
    solvent_opt_data(index,(k-1)*4+4)=fitparams(4);
end
total_solvent_info(index)=create_solvent_structure(solvent_opt_data,phi_toggle,index);
if phi_toggle==0
    fprintf(FID,['fiting parameters for static potential being excluded for ', num2str(solvent(index).Name),'\n']);
    fprintf(FID,'-------------------------------------------------------------------------\n');
    fprintf(FID,['Initial Guess: ',num2str(x_init),'\n']);
    fprintf(FID,'-------------------------------------------------------------------------\n');    
    fprintf(FID,'temp     alpha       beta       gamma          mu     phi_stat    \n');
    fprintf(FID,'-------------------------------------------------------------------------\n');    
    for kk=1:length(t)
        fprintf(FID,'%5.3f  %10.8f %10.8f %10.8f %10.8f %10.8f \n',t(kk),Params(kk,:,index),0);
    end
    fprintf(FID,'\n \n \n');
elseif phi_toggle==1      
    fprintf(FID,['fiting parameters including static potential for ', num2str(solvent(index).Name),'\n']);
    fprintf(FID,'-----------------------------------------------------------------------------------\n');
    fprintf(FID,['Initial Guess: ',num2str(x_init),'\n']);
    fprintf(FID,'-----------------------------------------------------------------------------------\n');    
    fprintf(FID,'temp      alpha          beta            gamma            mu          phi_stat    \n');
    fprintf(FID,'-----------------------------------------------------------------------------------\n');   
    for kk=1:length(t)
        fprintf(FID,'%5.3f    %10.8f    %10.8f    %10.8f      %10.8f      %10.8f \n',t(kk),Params(kk,:,index));
    end
    fprintf(FID,'\n \n \n');
end
for j=1:length(t)
    if phi_toggle==0
        error(j,:,index)=abs(Params(j,:,index)-[total_solvent_info(index).alpha_tanh_fit(t(j)),total_solvent_info(index).beta_tanh_fit(t(j)),total_solvent_info(index).gamma_tanh_fit(t(j)),total_solvent_info(index).mu_tanh_fit(t(j))]);
    elseif phi_toggle==1
        error(j,:,index)=abs(Params(j,:,index)-[total_solvent_info(index).alpha_tanh_fit(t(j)),total_solvent_info(index).beta_tanh_fit(t(j)),total_solvent_info(index).gamma_tanh_fit(t(j)),total_solvent_info(index).mu_tanh_fit(t(j)),total_solvent_info(index).phi_stat_tanh_fit(t(j))]);
    end
end
if errorgraph==1
    if phi_toggle==0
        figure
        plot(t,error(:,1,index),'-o','linewidth',2);
        legend('\alpha');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/O \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,2,index),'-o','linewidth',2);
        legend('\beta');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/O \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,3,index),'-o','linewidth',2);
        legend('\gamma');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/O \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,4,index),'-o','linewidth',2);
        legend('\mu');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/O \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);

    elseif phi_toggle==1
        figure
        plot(t,error(:,1,index),'-o','linewidth',2);
        legend('\alpha');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,2,index),'-o','linewidth',2);
        legend('\beta');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,3,index),'-o','linewidth',2);
        legend('\gamma');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        figure
        plot(t,error(:,4,index),'-o','linewidth',2);
        legend('\mu');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static} ', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
        plot(t,error(:,5,index),'-o','linewidth',2);
        legend('\mu');
        xlabel(['t ', total_solvent_info(index).Name]);
        ylabel(['error W/ \phi_{static}', total_solvent_info(index).Name]);
        set(gca,'fontsize',16);
    end
end

