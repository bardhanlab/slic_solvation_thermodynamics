function [  dphidn_HSBC_MSA,    sigma_over_dphidn_HSBC_MSA,     ...
            dphidn_NLBC_TANH,   sigma_nom_over_dphidn_NLBC_TANH,...
            dphidn_BORN,        sigma_over_dphidn_BORN,         ...
            DG_HSBC_MSA,        DS_HSBC_MSA,...
            DG_Fawcett_Asym,    DS_Fawcett_Asym,                dphidn_Fawcett_Asym,    sigma_over_dphidn_Fawcett_Asym,...
            DG_Fawcett_Sym,     DS_Fawcett_Sym,                 dphidn_Fawcett_Sym,     sigma_over_dphidn_Fawcett_Sym,...
            DG_NLBC_TANH,       DS_NLBC_TANH,...        
            DG_BORN,            DS_BORN,                        CP_BORN,...
	 Cp_NLBC_TANH]=Calcsolventmodels(index)
  

    tol=1e-15; 

    
    TSV = [];
    TEV = [];

global eps_in conv_factor total_solvent_info Ion_Data temp Ion_Color phi_toggle printOn hfuncgraph
temp=25;
    total_solvent_info=optimization_script_phi_toggle(index);


r=0.7:0.1:1000;   
TSV(index) = [total_solvent_info(index).Tmin];
TEV(index) = [total_solvent_info(index).Tmax];
TInterval=[TSV(index),TEV(index)];
eps_hat = ((total_solvent_info(index).eps_s(temp))-eps_in)/(total_solvent_info(index).eps_s(temp));
eps_hat_p=((total_solvent_info(index).deps_dt(temp))*(total_solvent_info(index).eps_s(temp))...
   -(total_solvent_info(index).deps_dt(temp))*((total_solvent_info(index).eps_s(temp))-eps_in))/((total_solvent_info(index).eps_s(temp)))^2;

Eps=total_solvent_info(index).eps_s(temp);
dEps_dT=total_solvent_info(index).deps_dt(temp);
deltaT = 1e-4;
d2Eps_dT2 = (total_solvent_info(index).deps_dt(temp+deltaT)- ...
     total_solvent_info(index).deps_dt(temp-deltaT))/(2*deltaT); % for now. 
eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
       - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

q=1;
[DG_p,DS_p,En_p,dphidn_p,Sigma_p]= HSBC_MSA(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor);  %Function for symmetric HSBC MSA

q=-1;
[DG_n,DS_n,En_n,dphidn_n,Sigma_n]= HSBC_MSA(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor);  %Function for symmetric HSBC MSA

dphidn_HSBC_MSA=[dphidn_p,dphidn_n(end:-1:1)];
sigma_over_dphidn_HSBC_MSA=[Sigma_p./dphidn_p,Sigma_n(end:-1:1)./dphidn_n(end:-1:1)];

q=1;
[DG_p,DS_p,En_p,dphidn_p,Sigma_p,Sigman_p]= NLBC_TANH(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor,eps_hat_p_p);  %Function for tanh

q=-1;
[DG_n,DS_n,En_n,dphidn_n,Sigma_n,Sigman_n]= NLBC_TANH(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor,eps_hat_p_p);  %Function for tanh

dphidn_NLBC_TANH=[dphidn_p,dphidn_n(end:-1:1)];
sigma_nom_over_dphidn_NLBC_TANH=[Sigman_p./dphidn_p,Sigman_n(end:-1:1)./dphidn_n(end:-1:1)];
%sigma_nom_over_dphidn_NLBC_TANH(:,i)=[Sigman_p,Sigman_n(end:-1:1)];
q=1;
[DG_p,DS_p,dphidn_p,Sigma_p]= BORN(eps_hat,eps_hat_p,eps_hat_p_p,temp,r,q,conv_factor);              %Function for Born

q=-1;
[DG_n,DS_n,dphidn_n,Sigma_n]= BORN(eps_hat,eps_hat_p,eps_hat_p_p,temp,r,q,conv_factor);              %Function for Born

dphidn_BORN= [dphidn_p,dphidn_n(end:-1:1)];
sigma_over_dphidn_BORN=[Sigma_p./dphidn_p,Sigma_n(end:-1:1)./dphidn_n(end:-1:1)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j=1:length(Ion_Data);
    q=Ion_Data(j).q;
    r=Ion_Data(j).r;
    [DG_HSBC_MSA(j),DS_HSBC_MSA(j),En(j),dphidn(j),Sigm(j)]= ...                             %Function for symmetric HSBC MSA
        HSBC_MSA(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor);

    [DG_Fawcett_Asym(j),DS_Fawcett_Asym(j),dphidn_Fawcett_Asym(j),Sigma_Fawcett_Asym(j)]= ...   %Asymmetric MSA for different Ions from Fawcett 92
        FAWCETT_ASYM_MSA(eps_hat,r,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor);

    sigma_over_dphidn_Fawcett_Asym(j)=Sigma_Fawcett_Asym(j)/dphidn_Fawcett_Asym(j);

    [DG_Fawcett_Sym(j),DS_Fawcett_Sym(j),dphidn_Fawcett_Sym(j),Sigma_Fawcett_Sym(j)]= ...   %Symmetric MSA for different Ions from Fawcett 92
        FAWCETT_SYM_MSA(eps_hat,r,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor);

    sigma_over_dphidn_Fawcett_Sym(j)=Sigma_Fawcett_Sym(j)/dphidn_Fawcett_Sym(j);

    [DG_NLBC_TANH(j),DS_NLBC_TANH(j),En_TANH(j),dphidn(j),Sigma(j),sigman_nlbc_tanh(j),Cp_NLBC_TANH(j)]= ...                               %Function for symmetric NLBC TANH
        NLBC_TANH(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor,eps_hat_p_p);

%     [DG_BORN(j),DS_BORN(j),dphidn(j),Sigma(j)]= ...                                         %Function for Born
%         BORN(eps_hat,eps_hat_p,r,q,conv_factor);
% end
    [DG_BORN(j),DS_BORN(j),dphidn(j),Sigma(j),CP_BORN(j)]= ...                                         %Function for Born
        BORN(eps_hat,eps_hat_p,eps_hat_p_p,temp,r,q,conv_factor);
end
if phi_toggle==1
Eps
end
if hfuncgraph==1
    if phi_toggle==1
        Eps
        EENN=-10:0.01:10;
        alpha =     total_solvent_info(index).alpha_tanh_fit(temp);
        beta  =     total_solvent_info(index).beta_tanh_fit(temp);
        gamma =     total_solvent_info(index).gamma_tanh_fit(temp);
        mu =        total_solvent_info(index).mu_tanh_fit(temp);
        hf_funct=alpha*tanh(beta*(EENN)-gamma)+mu;
        hf_valus=alpha*tanh(beta*(En_TANH)-gamma)+mu;
        map=[0,0,0; 1,0,0; 0.5, 0, 0; 0.5, 0.5, 0; 0, 0.5, 0; 0, 0.5, 1; 0, 0, 0.5; 0.5, 0, 0.5; 0.5, 0.5 0.5];
        hfig=figure
        plot(EENN,hf_funct,'linewidth',2,'Color',[0,0,0])
        hold on
        plot(EENN,1/eps_hat*ones(1,length(EENN)),'linewidth',4,'Color',[0.5,0.5,0.5])
        hold on
        scatter(En_TANH,hf_valus,400*ones(1,length(Ion_Color)),Ion_Color,'o','linewidth',3);
        colormap(map);
        %axis([min(En_TANH)*1.1 max(En_TANH)*1.1 min(hf_funct)-0.1 max(hf_funct)+0.1])
        axis([-0.4 1 0.2 1.2])
        ax = gca;
        ax.LineWidth = 1;
        ax.Box = 'on';
        set(gca,'fontsize',30);
        fig = gcf;
        fig.PaperUnits = 'inches';
        fig.PaperPosition = [0 0 6 3];
        set(hfig, 'Position', [100 100 1000 500]) 
        if printOn
          filename = sprintf('hfunc-%s.PDF',total_solvent_info(index).Name);
          export_fig(filename,'-painters','-transparent');
        end
    end
end
