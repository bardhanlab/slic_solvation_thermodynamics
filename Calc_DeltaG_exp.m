function [deltaG,eps_hat] = Calc_DeltaG_exp(temp,solvent,index)
eps_in=1;


global conv_factor Ion_Data EXP_G_dat EXP_S_dat removed_ion EXP_HEATCAP_dat   
delt = temp-25;

[Ion_Data,EXP_G_dat,EXP_S_dat,EXP_HEATCAP_dat,removed_ion] = Get_Ion_Data(solvent);

    for j=1:length(Ion_Data) 
        deltaG(index,j)=(EXP_G_dat(j)-delt*EXP_S_dat(j)/1000+EXP_HEATCAP_dat(j)/(1000)*(delt-(temp+273.15)*log((temp+273.15)/(25+273.15))))/conv_factor;
     end

deltaG = deltaG';
