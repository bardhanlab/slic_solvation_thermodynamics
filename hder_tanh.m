function [h_p,h_p_p]=hder_tanh(Tval,T,epsin,R,eps_out_f,q,alpha_f,beta_f,gamma_f,mu_f)
h=1e-4;

if T==Tval(1)
    T1=T;      
    epsout = eps_out_f(T1);
    alpha=alpha_f(T1);
    beta=beta_f(T1);
    gamma=gamma_f(T1);
    mu=mu_f(T1);
    epshat = (epsout-epsin)/epsout;
    dGdN=-q./R.^2;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;  % Induced surface charge based on HSBC/MSA model
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf1=alpha*tanh(beta*(E_n)-gamma)+mu;

    T2=T+h;     
    epsout = eps_out_f(T2);
    alpha=alpha_f(T2);
    beta=beta_f(T2);
    gamma=gamma_f(T2);
    mu=mu_f(T2);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf2=alpha*tanh(beta*(E_n)-gamma)+mu;
    
    T_ghost=T-h;     
    epsout = eps_out_f(T_ghost);
    alpha=alpha_f(T_ghost);
    beta=beta_f(T_ghost);
    gamma=gamma_f(T_ghost);
    mu=mu_f(T_ghost);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf_ghost=alpha*tanh(beta*(E_n)-gamma)+mu;
    
    h_p=(hf2-hf1)/h;
    h_p_p= (hf2 - 2 * hf1 + hf_ghost)/h^2;
    
elseif T==Tval(end)
    T1=T-h;   
    epsout = eps_out_f(T1);
    alpha=alpha_f(T1);
    beta=beta_f(T1);
    gamma=gamma_f(T1);
    mu=mu_f(T1);
    epshat = (epsout-epsin)/epsout;
    dGdN=-q./R.^2;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;  % Induced surface charge based on HSBC/MSA model
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf1=alpha*tanh(beta*(E_n)-gamma)+mu;

    T2=T;     
    epsout = eps_out_f(T2);
    alpha=alpha_f(T2);
    beta=beta_f(T2);
    gamma=gamma_f(T2);
    mu=mu_f(T2);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf2=alpha*tanh(beta*(E_n)-gamma)+mu;
    
    T_ghost=T+h;     
    epsout = eps_out_f(T_ghost);
    alpha=alpha_f(T_ghost);
    beta=beta_f(T_ghost);
    gamma=gamma_f(T_ghost);
    mu=mu_f(T_ghost);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf_ghost=alpha*tanh(beta*(E_n)-gamma)+mu;
    
    h_p=(hf2-hf1)/h;
    h_p_p=(hf1 - 2 * hf2 + hf_ghost)/h^2;
else
    T1=T-h;   
    epsout = eps_out_f(T1);
    alpha=alpha_f(T1);
    beta=beta_f(T1);
    gamma=gamma_f(T1);
    mu=mu_f(T1);
    epshat = (epsout-epsin)/epsout;
    dGdN=-q./R.^2;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;  % Induced surface charge based on HSBC/MSA model
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf1=alpha*tanh(beta*(E_n)-gamma)+mu;

    T2=T+h;     
    epsout = eps_out_f(T2);
    alpha=alpha_f(T2);
    beta=beta_f(T2);
    gamma=gamma_f(T2);
    mu=mu_f(T2);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf2=alpha*tanh(beta*(E_n)-gamma)+mu;
    
    epsout = eps_out_f(T);
    alpha=alpha_f(T);
    beta=beta_f(T);
    gamma=gamma_f(T);
    mu=mu_f(T);
    epshat = (epsout-epsin)/epsout;
    ERROR=1e10;
    s_NLBC0=epshat*dGdN;
    while ERROR>1e-12
        s_NLBC=s_NLBC0;
        E_n=-dGdN-(-0.5)*s_NLBC;
        hf=alpha*tanh(beta*(E_n)-gamma)+mu;
        s_NLBC0=epshat*dGdN/(1+hf);
        ERROR1=max(abs((s_NLBC0-s_NLBC)./s_NLBC0));
        ERROR2=max(abs(s_NLBC0-s_NLBC));
        ERROR=max(ERROR1,ERROR2);
    end
    s_NLBC=s_NLBC0;    
    E_n=-dGdN-(-0.5)*s_NLBC;
    hf0=alpha*tanh(beta*(E_n)-gamma)+mu;

    h_p=(hf2-hf1)/2/h;
    h_p_p = (hf2 - 2 * hf0 + hf1)/h^2;
end
    
    