function [DG,DS,dphidn,Sigma,CP]= BORN(eps_hat,eps_hat_p,eps_hat_p_p,t,r,q,conv_factor)

    dGdN=-1./r.^2;
    dphidn=-q./r.^2;
    Sigma= eps_hat * dGdN * q;
    DS=1000 * -0.5 * conv_factor * r .* dGdN * q^2 * eps_hat_p;
    DG = conv_factor * 0.5 * q * r .* Sigma;
    CP=(1000 * -0.5 * conv_factor * r .* dGdN * q^2 * eps_hat_p_p)*(t+273.15);

