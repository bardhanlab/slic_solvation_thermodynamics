function [DG,DS,En,dphidn,Sigma]= HSBC_MSA(eps_hat,eps_hat_p,r,tol,q,solventinfo,t,eps_in,TInterval,conv_factor)

    dGdN = -1./r.^2;
    sigma_0=eps_hat*q*dGdN;
    dphidn=-q./r.^2;
    alpha=solventinfo.alpha_f(t);
    ERROR=10;
    while ERROR>tol
        sigma=sigma_0;
        E_n_p=-q*dGdN-(-0.5)*sigma; 
        hf=alpha*sqrt(abs(E_n_p));
        sigma_0=eps_hat*q*dGdN./(1+hf);
        ERROR1=max(abs((sigma-sigma_0)./sigma_0));
        ERROR2=max(abs(sigma-sigma_0));
        ERROR=max(ERROR1,ERROR2);
    end
    Sigma=sigma_0;
    En=-q*dGdN-(-0.5)*Sigma;
    hf=solventinfo.alpha_f(t)*sqrt(abs(En));
    h_p=hder(TInterval,t,eps_in,r,solventinfo.eps_s,q,solventinfo.alpha_f); 
    DS=1000*-0.5*conv_factor*r.*dGdN*q^2.*(eps_hat_p.*(1+hf)-h_p.*eps_hat)./(1+hf).^2;
    DG=0.5*conv_factor*r.*Sigma*q;
