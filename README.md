Here are the matlab codes that have been developed for the results for the following paper: 

Predicting solvation thermodynamics with dielectric continuum theory and a solvation-layer interface condition (SLIC), 2016, Molavi Tabrizi et al.

To reproduce the figures and tables, do the followings:

1) Open main.m

2) If you want to save each individual figure automatically according o the names set inside the code, set printOn=1, otherwise set printOn=0.

3) If you want to create the free solvation energy figures, set deltaggraph=1, otherwise set deltaggraph=0.

4) If you want to create the solvation entropy figures set deltasgraph=1, otherwise set deltasgraph=0.

5) If you want to create the heat capacity figures set heatcapgraph=1, otherwise set heatcapgraph =0.

6) Choose the solvents you want to run the code for. This code works for water (abbreviated W), methanol (MeOH), ethanol (EtOH), formamide (F), acetonitrile (AN), dimethylformamide (DMF), dimethyl sulfoxide (DMSO), nitromethane (NM), and propylene carbonate (PC). If you want to run for all these solvents, set: solvent_type = {‘all’}; otherwise you can choose any other combination of solvents, for example solvent_type = {‘W’}; will run the code only for water, or solvent_type = {‘MeOH’,’AN’}; will run the code for methanol and acetonitrile.

7) Based on the above settings, this code creates the following outputs for each solvent

a) Free solvation energy figure
b) Entropy figure
c) Heat capacity figure
d) param_data.txt: A text file that reports the optimum values of parameters in different temperatures. W/ or W/O static potential.
e) parinfo.txt: A text file that report the value of each parameter at T=25C and its derivative with respect to temperature. Note that these values are only for the case that static potential has been included into the model.
f) predict.txt: In this file you can find the results for the ions that has not been used in parameterizing SLIC.