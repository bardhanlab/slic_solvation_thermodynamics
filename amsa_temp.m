function diff =  amsa_temp(x,tx,Ref_energy,index)
global phi_toggle eps_in

% Ions
global solvent Ion_Data


alpha = x(1);
beta = x(2);
gamma = x(3);
mu = x(4);
if phi_toggle==1
    phistat=x(5);
elseif phi_toggle==0
    phistat=0;
end

    eps_hat = ((solvent(index).eps_s(tx))-eps_in)/(solvent(index).eps_s(tx));
        for j = 1:length(Ion_Data)
            dGdN = -1./(Ion_Data(j).r).^2;
            dphidn_NLBC = -(Ion_Data(j).q)./(Ion_Data(j).r).^2;
            sigma_0 = eps_hat*Ion_Data(j).q*dGdN;
            ERROR = 1e10;
                while ERROR > 1e-10
                    sigma = sigma_0;
                    E_n = -(Ion_Data(j).q)*dGdN-(-0.5)*sigma; 
                    hf = alpha*tanh(beta*(E_n)-gamma)+mu;
                    sigma_0 = eps_hat*Ion_Data(j).q*dGdN./(1+hf);
                    ERROR1 = max(abs((sigma-sigma_0)./sigma_0));
                    ERROR2 = max(abs(sigma-sigma_0));
                    ERROR = max(ERROR1,ERROR2);
                end        
        sigma = sigma_0;
        DG(j,index) = 0.5*Ion_Data(j).r*Ion_Data(j).q*sigma+Ion_Data(j).q*phistat;
        end
%   diff = sum((DG(:,index)-Ref_energy(:,index)).^2);
diff = DG(:,index)-Ref_energy(:,index);