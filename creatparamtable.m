function creatparamtable(solventinfo,phi_toggle)

global FID2 printOn

alpha =     solventinfo.alpha_tanh_fit(25);
beta  =     solventinfo.beta_tanh_fit(25);
gamma =     solventinfo.gamma_tanh_fit(25);
mu =        solventinfo.mu_tanh_fit(25);
phi_stat=   solventinfo.phi_stat_tanh_fit(25);

alphaf =     solventinfo.alpha_tanh_fit;
betaf  =     solventinfo.beta_tanh_fit;
gammaf =     solventinfo.gamma_tanh_fit;
muf =        solventinfo.mu_tanh_fit;
phi_statf=   solventinfo.phi_stat_tanh_fit;

alpha_der=(alphaf(25+1e-4)-alphaf(25-1e-4))/(2e-4);
beta_der=(betaf(25+1e-4)-betaf(25-1e-4))/(2e-4);
gamma_der=(gammaf(25+1e-4)-gammaf(25-1e-4))/(2e-4);
mu_der=(muf(25+1e-4)-muf(25-1e-4))/(2e-4);
phistat_der=(phi_statf(25+1e-4)-phi_statf(25-1e-4))/(2e-4);

if phi_toggle==2
    fprintf(FID2,'\n \n \n');
    fprintf(FID2,['Parameters W/O phi_{static} in ', num2str(solventinfo.Name),'\n']);
    fprintf(FID2,'-------------------------------------------------------------------------\n');
    fprintf(FID2,'-------------------------------------------------------------------------\n');    
    fprintf(FID2,'alpha      dalpha_dt       beta      dbeta_dt       gamma      dgamma_dt       mu      dmu_dt        phi_stat      dphistat_dt\n');
    fprintf(FID2,'-------------------------------------------------------------------------\n');
    fprintf(FID2,'%6.3e %6.3e %6.3e %6.3e %6.3e %6.3e %6.3e %6.3e %6.3e %6.3e\n',alpha,alpha_der,beta,beta_der,gamma,gamma_der,mu,mu_der,phi_stat,phistat_der);

elseif phi_toggle==1      
    fprintf(FID2,'\n \n \n');
    fprintf(FID2,['Parameters W phi_{static} in ', num2str(solventinfo.Name),'\n']);
    fprintf(FID2,'-------------------------------------------------------------------------\n');
    fprintf(FID2,'-------------------------------------------------------------------------\n');    
    fprintf(FID2,'alpha        dalpha_dt         beta        dbeta_dt         gamma        dgamma_dt         mu        dmu_dt         phi_stat        dphistat_dt\n');
    fprintf(FID2,'-------------------------------------------------------------------------\n');
    fprintf(FID2,'%6.3e      %6.3e      %6.3e      %6.3e      %6.3e      %6.3e      %6.3e      %6.3e      %6.3e      %6.3e\n',alpha,alpha_der,beta,beta_der,gamma,gamma_der,mu,mu_der,phi_stat,phistat_der);
end

tmin=solventinfo.Tmin;
tmax=solventinfo.Tmax;
dt=(tmax-tmin)/100;
tt=tmin:dt:tmax;
for i=1:length(tt)
    alpha_a(i)=alphaf(tt(i));
    beta_a(i)=betaf(tt(i));
    gamma_a(i)=gammaf(tt(i));
    mu_a(i)=muf(tt(i));
    phistat_a(i)=phi_statf(tt(i));
end

% if phi_toggle==1
%     figure;
%         plot(tt,alpha_a,'linewidth',2,'Color',[0.5,0,0])
%         ax = gca;
%         ax.LineWidth = 1;
%         ax.Box = 'on';
%         set(gca,'fontsize',30);
%         fig = gcf;
%         fig.PaperUnits = 'inches';
%         fig.PaperPosition = [0 0 6 6];
%         xlabel(['T'])
%         ylabel(['\alpha'])
%         axis([tmin-2 tmax+2 0.95*min(alpha_a) 1.05*max(alpha_a)])
%         if printOn
%             filename = sprintf('alpha-%s.pdf',solventinfo.Name);
%             export_fig(filename,'-painters','-transparent');
%         end
% 
%     figure;        
%         plot(tt,beta_a,'linewidth',2,'Color',[0.5,0.5,0])
%         ax = gca;
%         ax.LineWidth = 1;
%         ax.Box = 'on';
%         set(gca,'fontsize',30);
%         fig = gcf;
%         fig.PaperUnits = 'inches';
%         fig.PaperPosition = [0 0 6 6];
%         xlabel(['T'])
%         ylabel(['\beta'])
%         axis([tmin-2 tmax+2 0.95*min(beta_a) 1.05*max(beta_a)])
%         if printOn
%             filename = sprintf('beta-%s.pdf',solventinfo.Name);
%             export_fig(filename,'-painters','-transparent');
%         end
%         
%     figure;        
%         plot(tt,gamma_a,'linewidth',2,'Color',[0,0.5,1])
%         ax = gca;
%         ax.LineWidth = 1;
%         ax.Box = 'on';
%         set(gca,'fontsize',30);
%         fig = gcf;
%         fig.PaperUnits = 'inches';
%         fig.PaperPosition = [0 0 6 6];
%         xlabel(['T'])
%         ylabel(['\gamma'])
%         axis([tmin-2 tmax+2 0.95*min(gamma_a) 1.05*max(gamma_a)])
%         if printOn
%             filename = sprintf('gamma-%s.pdf',solventinfo.Name);
%             export_fig(filename,'-painters','-transparent');
%         end
%         
%     figure;
%         plot(tt,mu_a,'linewidth',2,'Color',[0,0,0.5])
%         ax = gca;
%         ax.LineWidth = 1;
%         ax.Box = 'on';
%         set(gca,'fontsize',30);
%         fig = gcf;
%         fig.PaperUnits = 'inches';
%         fig.PaperPosition = [0 0 6 6];
%         xlabel(['T'])
%         ylabel(['\mu'])
%         axis([tmin-2 tmax+2 0.95*min(mu_a) 1.05*max(mu_a)])
%         if printOn
%             filename = sprintf('mu-%s.pdf',solventinfo.Name);
%             export_fig(filename,'-painters','-transparent');
%         end
%         
%     figure;
%         plot(tt,phistat_a,'linewidth',2,'Color',[0.5,0,0.5])
%         ax = gca;
%         ax.LineWidth = 1;
%         ax.Box = 'on';
%         set(gca,'fontsize',30);
%         fig = gcf;
%         fig.PaperUnits = 'inches';
%         fig.PaperPosition = [0 0 6 6];
%         xlabel(['T'])
%         ylabel(['\phi^{stat}'])
%         axis([tmin-2 tmax+2 1.05*min(phistat_a) 0.95*max(phistat_a)])
%         if printOn
%             filename = sprintf('phistat-%s.pdf',solventinfo.Name);
%             export_fig(filename,'-painters','-transparent');
%         end
% end
%     

%    axis([min(EXP_G_dat)-20 max(EXP_G_dat)+20 min(min(TABLE_G(:,:,i)))-20 max(max(TABLE_G(:,:,i)))+20])
    %axis([min(min(TABLE_G(:,:,i)))-10 max(max(TABLE_G(:,:,i)))+10 min(min(TABLE_G(:,:,i)))-10 max(max(TABLE_G(:,:,i)))+10])

     
%    if printOn
%      filename = sprintf('deltaG-%s.PDF',solvent(i).Name);
%      export_fig(filename,'-painters','-transparent');
%    end
%end






